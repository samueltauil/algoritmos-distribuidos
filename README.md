# Information #

Here are some exercises for the Distributed Algorithms in UFSCar.
All of them are developed using [Appia](http://appia.di.fc.ul.pt/wiki/index.php?title=Main_Page) framework.

Projects already have the log4j dependency added to their maven configuration file. But **you must add the appia-core jar library** to the project classpath to get examples running properly.

They follow the book [Introduction to Reliable and Secure Distributed Programming](http://www.amazon.com/Introduction-Reliable-Secure-Distributed-Programming/dp/3642152597).

### Any information needed? ###
* Suggestion, fixes, improvements contact: [samueltauil@gmail.com](mailto:samueltauil@gmail.com)