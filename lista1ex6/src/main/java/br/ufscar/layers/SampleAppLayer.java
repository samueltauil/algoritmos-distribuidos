package br.ufscar.layers;

import br.ufscar.events.ProcessInitEvent;
import br.ufscar.events.SampleSendableEvent;
import br.ufscar.sessions.SampleAppSession;
import net.sf.appia.core.Layer;
import net.sf.appia.core.Session;
import net.sf.appia.core.events.channel.ChannelClose;
import net.sf.appia.core.events.channel.ChannelInit;
import net.sf.appia.protocols.common.RegisterSocketEvent;

/**
 * Created by samueltauil on 4/1/15.
 */
public class SampleAppLayer extends Layer {

    public SampleAppLayer(){
                 /* events that the protocol will create */
        evProvide = new Class[3];
        evProvide[0] = ProcessInitEvent.class;
        evProvide[1] = RegisterSocketEvent.class;
        evProvide[2] = SampleSendableEvent.class;

    /*
     * events that the protocol require to work. This is a subset of the
     * accepted events
     */
        evRequire = new Class[0];


    /* events that the protocol will accept */
        evAccept = new Class[4];
        evAccept[0] = ChannelInit.class;
        evAccept[1] = ChannelClose.class;
        evAccept[2] = RegisterSocketEvent.class;
        evAccept[3] = SampleSendableEvent.class;
    }

    /**
     * Creates a new session to this protocol.
     *
     *
     */
    public Session createSession() {
        return new SampleAppSession(this);
    }
}
