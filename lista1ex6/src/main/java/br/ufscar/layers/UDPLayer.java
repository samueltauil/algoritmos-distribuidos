package br.ufscar.layers;

import br.ufscar.events.ProcessInitEvent;
import br.ufscar.sessions.UDPSession;
import net.sf.appia.core.Layer;
import net.sf.appia.core.Session;
import net.sf.appia.core.events.SendableEvent;
import net.sf.appia.core.events.channel.ChannelClose;
import net.sf.appia.core.events.channel.ChannelInit;

/**
 * Created by samueltauil on 4/1/15.
 */
public class UDPLayer extends Layer{

    private int fanout;
    private int maxRounds;

    public UDPLayer() {
    /* events that the protocol will create */
        evProvide = new Class[0];

    /*
     * events that the protocol require to work. This is a subset of the
     * accepted events
     */
        evRequire = new Class[0];

    /* events that the protocol will accept */
        evAccept = new Class[4];
        evAccept[0] = SendableEvent.class;
        evAccept[1] = ChannelInit.class;
        evAccept[2] = ChannelClose.class;
        evAccept[3] = ProcessInitEvent.class;
    }

    /**
     * Creates a new session to this protocol.
     *
     *
     */
    public Session createSession() {
        return new UDPSession(this);
    }

    /**
     * Sets the values that will be passed to the Session.
     *
     * @param f
     *          fanout
     * @param r
     *          number of rounds
     */
    public void initValues(int f, int r) {
        fanout = f;
        maxRounds = r;
    }

    /**
     * Gets the fanout.
     *
     * @return the fanout
     */
    public int getFanout() {
        return fanout;
    }

    /**
     * Gets the maximum number of rounds.
     *
     * @return the maximum number of rounds
     */
    public int getMaxRounds() {
        return maxRounds;
    }

}
