package br.ufscar.utils;

/**
 * Created by samueltauil on 4/1/15.
 */
public class Debug {
    private final static boolean debugOn = false;

    public static void print(String message) {
        if (debugOn)
            System.err.println(message);
    }
}
