package br.ufscar.utils;

import java.io.Serializable;

/**
 * Created by samueltauil on 4/1/15.
 */
public class MessageID implements Serializable {
    private static final long serialVersionUID = -5632927438973377599L;

    public int process, seqNumber;
    public MessageID(int p, int s) {
        process = p;
        seqNumber = s;
    }
    public int hashCode() {
        return process ^ seqNumber;
    }
    public boolean equals(Object o) {
        MessageID id = (MessageID) o;
        return process == id.process && seqNumber == id.seqNumber;
    }

    public String toString() {
        return " (" + process + "," + seqNumber + ") ";
    }
}
