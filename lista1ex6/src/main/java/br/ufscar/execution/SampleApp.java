package br.ufscar.execution;

import br.ufscar.layers.SampleAppLayer;
import br.ufscar.layers.UDPLayer;
import br.ufscar.sessions.SampleAppSession;
import br.ufscar.utils.ProcessSet;
import br.ufscar.utils.SampleProcess;
import net.sf.appia.core.*;
import net.sf.appia.protocols.udpsimple.UdpSimpleLayer;

import java.io.*;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.StringTokenizer;

/**
 * Created by samueltauil on 4/1/15.
 */
public class SampleApp {
    /**
     * Builds the Process set, using the information in the specified file.
     *
     * @param filename
     *          the location of the file
     * @param selfProc
     *          the number of the self process
     * @return a new ProcessSet
     */
    private static ProcessSet buildProcessSet(String filename, int selfProc) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(
                    filename)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.exit(0);
        }
        String line;
        StringTokenizer st;
        boolean hasMoreLines = true;
        ProcessSet set = new ProcessSet();
        // reads lines of type: <process number> <IP address> <port>
        while(hasMoreLines) {
            try {
                line = reader.readLine();
                if (line == null)
                    break;
                st = new StringTokenizer(line);
                if (st.countTokens() != 3) {
                    System.err.println("Wrong line in file: "+st.countTokens());
                    continue;
                }
                int procNumber = Integer.parseInt(st.nextToken());
                InetAddress addr = InetAddress.getByName(st.nextToken());
                int portNumber = Integer.parseInt(st.nextToken());
                boolean self = (procNumber == selfProc);
                SampleProcess process = new SampleProcess(new InetSocketAddress(addr,
                        portNumber), procNumber, self);
                set.addProcess(process, procNumber);
            } catch (IOException e) {
                hasMoreLines = false;
            } catch (NumberFormatException e) {
                System.err.println(e.getMessage());
            }
        } // end of while
        return set;
    }

    /**
     * Builds a new Channel with Probabilistic Broadcast.
     *
     * @param processes
     *          set of processes
     * @param fanout
     *          fanout to use in the protocol
     * @param rounds
     *          number of rounds to use in the protocol
     * @return a new uninitialized Channel
     */
    private static Channel getPBChannel(ProcessSet processes, int fanout,
                                        int rounds) {
    /* Creates a new PBLayer and initializes it */
        UDPLayer pbLayer = new UDPLayer();
        pbLayer.initValues(fanout, rounds);
    /* Create layers and put them on a array */
        Layer[] qos = {new UdpSimpleLayer(), pbLayer, new SampleAppLayer()};

    /* Create a QoS */
        QoS myQoS = null;
        try {
            myQoS = new QoS("Probabilistic Broadcast QoS", qos);
        } catch (AppiaInvalidQoSException ex) {
            System.err.println("Invalid QoS");
            System.err.println(ex.getMessage());
            System.exit(1);
        }
    /* Create a channel. Uses default event scheduler. */
        Channel channel = myQoS
                .createUnboundChannel("Probabilistic Broadcast Channel");
    /*
     * Application Session requires special arguments: filename and . A session
     * is created and binded to the stack. Remaining ones are created by default
     */
        SampleAppSession sas = (SampleAppSession) qos[qos.length - 1]
                .createSession();
        sas.init(processes);
        ChannelCursor cc = channel.getCursor();
    /*
     * Application is the last session of the array. Positioning in it is simple
     */
        try {
            cc.top();
            cc.setSession(sas);
        } catch (AppiaCursorException ex) {
            System.err.println("Unexpected exception in main. Type code:" + ex.type);
            System.exit(1);
        }
        return channel;
    }



    public static void main(String[] args) {

        int fanout = 0;
        int rounds = 0;
    /* Parse arguments */
        int arg = 0, self = -1;
        String filename = null, qos = null;
        try {
            while (arg < args.length) {
                if (args[arg].equals("-f")) {
                    arg++;
                    filename = args[arg];
                    System.out.println("Reading from file: " + filename);
                } else if (args[arg].equals("-n")) {
                    arg++;
                    try {
                        self = Integer.parseInt(args[arg]);
                        System.out.println("Process number: " + self);
                    } catch (NumberFormatException e) {
                        invalidArgs(e.getMessage());
                    }
                } else if (args[arg].equals("-qos")) {
                    arg++;
                    qos = args[arg];
                    if (qos.equals("pb")) {




                        try {
                            fanout = Integer.parseInt(args[++arg]);
                            rounds = Integer.parseInt(args[++arg]);
                        } catch (NumberFormatException e) {
                            invalidArgs(e.getMessage());
                        }
                    }
                    qos = qos + " " + fanout + " " + rounds;
                    System.out.println("Starting with QoS: " + qos);
                } else
                    invalidArgs("Unknown argument: "+args[arg]);
                arg++;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            invalidArgs(e.getMessage());
        }

    /*
     * gets a new uninitialized Channel with the specified QoS and the Appl
     * session created. Remaining sessions are created by default. Just tell the
     * channel to start.
     */
        Channel channel = getPBChannel(buildProcessSet(filename, self), fanout, rounds);
        try {
            channel.start();
        } catch (AppiaDuplicatedSessionsException ex) {
            System.err.println("Sessions binding strangely resulted in "
                    + "one single sessions occurring more than " + "once in a channel");
            System.exit(1);
        }

    /* All set. Appia main class will handle the rest */
        System.out.println("Starting Appia...");
        Appia.run();
    }

    /**
     * Prints a error message and exit.
     * @param reason the reason of the failure
     */
    private static void invalidArgs(String reason) {
        System.out
                .println("Invalid args: "+reason+"\nUsage SampleAppl -f filemane -n proc_number -qos QoS_type."
                        + "\n QoS can be one of the following:"
                        + "\n\t beb - Best Effort Broadcast"
                        + "\n\t rb - Lazy Reliable Broadcast"
                        + "\n\t urb - All-Ack Uniform Reliable Broadcast"
                        + "\n\t iurb - Majority-Ack Uniform Reliable Broadcast"
                        + "\n\t pb <f> <r> - Probabilistic Broadcast with a fanout f and a number of rounds r"
                        + "\n\t fc - Flooding Consensus"
                        + "\n\t hc - Hierarchical Consensus"
                        + "\n\t ufc - Uniform Flooding Consensus"
                        + "\n\t uhc - Uniform Hierarchical Consensus"
                        +
                        // "\n\t clc - Careful Leader Consensus"+
                        "\n\t conow - No-Waiting Casual Order"
                        + "\n\t conowgc - No-Waiting Casual Order with Garbage Collection"
                        + "\n\t cow - Waiting Casual Order"
                        + "\n\t uto - Consensus-Based Uniform Total Order"
                        + "\n\t r1nr - Read-One-Write-All Regular (1,N) Register"
                        + "\n\t a1nr - Read-Impose-Write-All Atomic (1,N) Register"
                        + "\n\t annr - Read-Impose Write-Consult Atomic (N,N) Register"
                        + "\n\t nbac - Consensus-based Non-Blocking Atomic Commit"
                        + "\n\t cmem - Consensus-based Membership"
                        + "\n\t trbvs - TRB-based View Synchrony");
        System.exit(1);
    }
}
