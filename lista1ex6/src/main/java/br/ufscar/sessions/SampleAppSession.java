package br.ufscar.sessions;

import br.ufscar.events.ProcessInitEvent;
import br.ufscar.events.SampleSendableEvent;
import br.ufscar.utils.ProcessSet;
import br.ufscar.utils.SampleAppReader;
import net.sf.appia.core.*;
import net.sf.appia.core.events.channel.ChannelClose;
import net.sf.appia.core.events.channel.ChannelInit;
import net.sf.appia.protocols.common.RegisterSocketEvent;

import java.net.InetSocketAddress;

/**
 * Created by samueltauil on 4/1/15.
 */
public class SampleAppSession extends Session {

    public Channel channel;
    private ProcessSet processes;
    private SampleAppReader reader;
    private boolean blocked = false;

    public SampleAppSession(Layer layer) {
        super(layer);
    }

    public void init(ProcessSet processes) {
        this.processes = processes;
    }

    /**
     * @param event
     */
    private void handleRegisterSocket(RegisterSocketEvent event) {
        if (event.error) {
            System.out.println("Address already in use!");
            System.exit(2);
        }
    }

    /**
     * @param init
     */
    private void handleChannelInit(ChannelInit init) {
        try {
            init.go();
        } catch (AppiaEventException e) {
            e.printStackTrace();
        }
        channel = init.getChannel();

        try {
            // sends this event to open a socket in the layer that is used has perfect
            // point to point
            // channels or unreliable point to point channels.
            RegisterSocketEvent rse = new RegisterSocketEvent(channel,
                    Direction.DOWN, this);
            rse.port = ((InetSocketAddress) processes.getSelfProcess().getSocketAddress()).getPort();
            rse.localHost = ((InetSocketAddress)processes.getSelfProcess().getSocketAddress()).getAddress();
            rse.go();
            ProcessInitEvent processInit = new ProcessInitEvent(channel,
                    Direction.DOWN, this);
            processInit.setProcessSet(processes);
            processInit.go();
        } catch (AppiaEventException e1) {
            e1.printStackTrace();
        }
        System.out.println("Channel is open.");
        // starts the thread that reads from the keyboard.
        reader = new SampleAppReader(this);
        reader.start();
    }

    /**
     * @param close
     */
    private void handleChannelClose(ChannelClose close) {
        channel = null;
        System.out.println("Channel is closed.");
    }


    public void handle(Event event) {
        if (event instanceof SampleSendableEvent)
            handleSampleSendableEvent((SampleSendableEvent) event);
        else if (event instanceof ChannelInit)
            handleChannelInit((ChannelInit) event);
        else if (event instanceof ChannelClose)
            handleChannelClose((ChannelClose) event);
        else if (event instanceof RegisterSocketEvent)
            handleRegisterSocket((RegisterSocketEvent) event);
    }


    /**
     * @param event
     */
    private void handleSampleSendableEvent(SampleSendableEvent event) {
        if (event.getDir() == Direction.DOWN)
            handleOutgoingEvent(event);
        else
            handleIncomingEvent(event);
    }

    private void handleIncomingEvent(SampleSendableEvent event) {
        String message = event.getMessage().popString();
        System.out.print("Received event with message: " + message + "\n>");
    }

    /**
     * @param event
     */
    private void handleOutgoingEvent(SampleSendableEvent event) {
        String command = event.getCommand();
        if ("bcast".equals(command))
            handleBCast(event);
        else if ("help".equals(command))
            printHelp();
        else {
            System.out.println("Invalid command: " + command);
            printHelp();
        }
    }

    /**
     * @param event
     */
    private void handleBCast(SampleSendableEvent event) {
        if (blocked) {
            System.out
                    .println("The group is blocked, therefore message can not be sent.");
            return;
        }

        try {
            event.go();
        } catch (AppiaEventException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     */
    private void printHelp() {
        System.out
                .println("Available commands:\n"
                        + "bcast <msg> - Broadcast the message \"msg\"\n"
                        + "help - Print this help information.");
    }
}
