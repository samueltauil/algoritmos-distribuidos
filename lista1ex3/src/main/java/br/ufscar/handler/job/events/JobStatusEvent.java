package br.ufscar.handler.job.events;

import net.sf.appia.core.Event;

/**
 * Created by samueltauil on 3/25/15.
 */
public class JobStatusEvent extends Event {

    int id;
    Status status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
