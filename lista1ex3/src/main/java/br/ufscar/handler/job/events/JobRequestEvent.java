package br.ufscar.handler.job.events;

import net.sf.appia.core.Event;

/**
 * Created by samueltauil on 3/25/15.
 */
public class JobRequestEvent extends Event {

    int id;
    String string;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

}
