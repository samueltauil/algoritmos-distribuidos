package br.ufscar.handler.job.sessions;

import br.ufscar.handler.job.events.JobConfirmEvent;
import br.ufscar.handler.job.events.JobRequestEvent;
import net.sf.appia.core.*;
import net.sf.appia.core.events.channel.ChannelInit;

/**
 * Created by samueltauil on 3/24/15.
 */
public class JobHandlerSession extends Session {

    public JobHandlerSession(Layer layer) {
        super(layer);
    }

    @Override
    public void handle(Event event) {

        if (event instanceof ChannelInit){
            handleChannelInit((ChannelInit) event);
        } else if (event instanceof JobRequestEvent){
            handleJobRequest((JobRequestEvent) event);
        }

    }

    private void handleJobRequest(JobRequestEvent request) {

        try {
            JobConfirmEvent ack = new JobConfirmEvent();

            System.out.println(request.getString());

            ack.setChannel(request.getChannel());
            ack.setDir(Direction.DOWN);
            ack.setSourceSession(this);
            ack.setId(request.getId());
            ack.init();
            ack.go();

        } catch (AppiaEventException e) {
            e.printStackTrace();
        }
    }

    private void handleChannelInit(ChannelInit init) {
        try {
           init.go();
        } catch (AppiaEventException e) {
            e.printStackTrace();
        }
    }
}
