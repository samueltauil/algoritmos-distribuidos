package br.ufscar.handler.job.layers;

import br.ufscar.handler.job.events.JobConfirmEvent;
import br.ufscar.handler.job.events.JobRequestEvent;
import br.ufscar.handler.job.events.JobStatusEvent;
import br.ufscar.handler.job.sessions.JobHandlerApplicationSession;
import net.sf.appia.core.Layer;
import net.sf.appia.core.Session;
import net.sf.appia.core.events.channel.ChannelInit;

/**
 * Created by samueltauil on 3/25/15.
 */
public class JobHandlerApplicationLayer extends Layer {

    public JobHandlerApplicationLayer(){

        evProvide = new Class[1];
        evProvide[0] = JobRequestEvent.class;

        evRequire = new Class[0];

        evAccept = new Class[3];
        evAccept[0] = JobConfirmEvent.class;
        evAccept[1] = JobStatusEvent.class;
        evAccept[2] = ChannelInit.class;

    }

    @Override
    public Session createSession() {
        return new JobHandlerApplicationSession(this);
    }
}
