package br.ufscar.handler.job.events;

import net.sf.appia.core.Event;

/**
 * Created by samueltauil on 3/25/15.
 */
public class JobConfirmEvent extends Event {

    int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
