package br.ufscar.handler.job.sessions;

import br.ufscar.handler.job.events.JobConfirmEvent;
import br.ufscar.handler.job.events.JobRequestEvent;
import br.ufscar.handler.job.events.JobStatusEvent;
import br.ufscar.handler.job.events.Status;
import net.sf.appia.core.*;
import net.sf.appia.core.events.channel.ChannelInit;

/**
 * Created by samueltauil on 3/25/15.
 */
public class TransformationHandlerSession extends Session {

    int top;
    int bottom;
    boolean handling;
    int M;
    JobRequestEvent[] buffer;

    public TransformationHandlerSession(Layer layer) {
        super(layer);
    }

    @Override
    public void handle(Event event) {

        if (event instanceof ChannelInit) {
            handleChannelInit((ChannelInit) event);
        } else if (event instanceof JobRequestEvent) {
            handleJobRequest((JobRequestEvent) event);
        } else if (event instanceof JobConfirmEvent) {
            handleConfirmRequest((JobConfirmEvent) event);
        }
    }

    private void handleConfirmRequest(JobConfirmEvent confirm) {
        handling = false;
        //buffer can be cleaned to implement a real fifo queue.
    }

    private void handleJobRequest(JobRequestEvent request) {

        try {
                JobStatusEvent status = new JobStatusEvent();
                status.setChannel(request.getChannel());
                status.setSourceSession(this);
                status.setDir(Direction.UP);
                status.setId(request.getId());

            if (bottom + M == top) {

                  status.setStatus(Status.NOK);
                  status.init();
                  status.go();

              } else {

                  try {
                      buffer[top % M + 1] = request;
                  } catch (ArrayIndexOutOfBoundsException e) {
                      status.setStatus(Status.NOK);
                      status.init();
                      status.go();
                  }
                  top++;
                  status.setStatus(Status.OK);
                  status.init();
                  status.go();

              }

            if (bottom < top || !handling) {
                try {
                    request = buffer[bottom % M + 1];
                    bottom++;
                    handling = true;

                    System.out.println(request.getString().toUpperCase());
                    request.setDir(Direction.UP);
                    request.init();
                    request.go();

                } catch (ArrayIndexOutOfBoundsException e) {
                    status.setStatus(Status.NOK);
                    status.init();
                    status.go();

                }

            }

        } catch (AppiaEventException e) {
            e.printStackTrace();
        }
    }

    private void handleChannelInit(ChannelInit init) {

        try {
            top = 0;
            bottom = 0;
            handling = false;
            M = 15;
            buffer = new JobRequestEvent[M];

            init.go();
        } catch (AppiaEventException e) {
            e.printStackTrace();
        }
    }
}
