package br.ufscar.handler.job.execution;

import br.ufscar.handler.job.layers.JobHandlerApplicationLayer;
import br.ufscar.handler.job.layers.JobHandlerLayer;
import br.ufscar.handler.job.layers.TransformationHandlerLayer;
import net.sf.appia.core.*;

/**
 * Created by samueltauil on 3/25/15.
 */
public class Example {

    public static void main(String[] args) {
        Layer[] qos =
                {new JobHandlerLayer(),
                 new TransformationHandlerLayer(),
                 new JobHandlerApplicationLayer()};

        QoS myQoS = null;

        try {
            myQoS = new QoS("JobHandler_stack",qos);
        } catch (AppiaInvalidQoSException e) {
            System.err.println("Invalid QoS");
            System.err.println(e.getMessage());
            System.exit(1);
        }

        Channel channel = myQoS.createUnboundChannel("JobHandler_channel");

        try {
            channel.start();
        } catch (AppiaDuplicatedSessionsException e) {
            System.err.println("Error in Channel start");
            System.exit(1);
        }

        System.out.println("Starting Appia...");
        Appia.run();
    }
}
