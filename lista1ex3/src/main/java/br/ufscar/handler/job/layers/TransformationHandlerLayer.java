package br.ufscar.handler.job.layers;

import br.ufscar.handler.job.events.JobConfirmEvent;
import br.ufscar.handler.job.events.JobRequestEvent;
import br.ufscar.handler.job.events.JobStatusEvent;
import br.ufscar.handler.job.sessions.TransformationHandlerSession;
import net.sf.appia.core.Layer;
import net.sf.appia.core.Session;
import net.sf.appia.core.events.channel.ChannelInit;

/**
 * Created by samueltauil on 3/25/15.
 */
public class TransformationHandlerLayer extends Layer{

    public TransformationHandlerLayer(){
        evProvide = new Class[1];
        evProvide[0] = JobStatusEvent.class;

        evRequire = new Class[0];

        evAccept = new Class[3];
        evAccept[0] = JobRequestEvent.class;
        evAccept[1] = JobConfirmEvent.class;
        evAccept[2] = ChannelInit.class;
    }

    @Override
    public Session createSession() {
        return new TransformationHandlerSession(this);
    }
}
