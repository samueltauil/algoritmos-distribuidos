package br.ufscar.handler.job.layers;

import br.ufscar.handler.job.sessions.JobHandlerSession;
import br.ufscar.handler.job.events.JobConfirmEvent;
import br.ufscar.handler.job.events.JobRequestEvent;
import net.sf.appia.core.Layer;
import net.sf.appia.core.Session;
import net.sf.appia.core.events.channel.ChannelInit;

/**
 * Created by samueltauil on 3/24/15.
 */
public class JobHandlerLayer extends Layer {

    public JobHandlerLayer(){
        evProvide = new Class[1];
        evProvide[0] = JobConfirmEvent.class;

        evRequire = new Class[0];

        evAccept = new Class[2];
        evAccept[0] = JobRequestEvent.class;
        evAccept[1] = ChannelInit.class;
    }

    @Override
    public Session createSession() {
        return new JobHandlerSession(this);
    }
}
