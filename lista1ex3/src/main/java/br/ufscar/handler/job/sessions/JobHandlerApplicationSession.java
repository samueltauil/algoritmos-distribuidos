package br.ufscar.handler.job.sessions;

import br.ufscar.handler.job.events.JobConfirmEvent;
import br.ufscar.handler.job.events.JobRequestEvent;
import br.ufscar.handler.job.events.JobStatusEvent;
import br.ufscar.handler.job.events.Status;
import net.sf.appia.core.*;
import net.sf.appia.core.events.channel.ChannelInit;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by samueltauil on 3/25/15.
 */
public class JobHandlerApplicationSession extends Session {

    private PrintReader reader = null;

    public JobHandlerApplicationSession(Layer layer) {
        super(layer);
    }

    @Override
    public void handle(Event event) {
        if (event instanceof ChannelInit)
            handleChannelInit((ChannelInit) event);
        else if (event instanceof JobConfirmEvent)
            handleJobConfirm((JobConfirmEvent) event);
        else if (event instanceof JobStatusEvent)
            handleJobStatus((JobStatusEvent) event);
    }

    private void handleChannelInit(ChannelInit init) {
        try {
            init.go();
        } catch (AppiaEventException ex) {
            ex.printStackTrace();
        }

        if (reader == null)
            reader = new PrintReader(init.getChannel());
    }

    private void handleJobConfirm(JobConfirmEvent conf) {
        System.out.println("[JobHandlerApplication: received confirmation of request "
                + conf.getId() + "]");

        try {
            conf.go();
        } catch (AppiaEventException ex) {
            ex.printStackTrace();
        }
    }

    private void handleJobStatus(JobStatusEvent status) {
        System.out.print("[JobHandlerApplication: received");
        System.out.print(" status "
                + (status.getStatus().equals(Status.OK) ? "OK" : "NOK"));
        System.out.println(" for request " + status.getId() + "]");

        try {
            status.go();
        } catch (AppiaEventException ex) {
            ex.printStackTrace();
        }
    }


    private class PrintReader extends Thread {

        public boolean ready = false;
        public Channel channel;
        private BufferedReader stdin = new BufferedReader(new InputStreamReader(
                System.in));
        private int rid = 0;

        public PrintReader(Channel channel) {
            ready = true;
            if (this.channel == null)
                this.channel = channel;
            this.start();
        }

        public void run() {
            boolean running = true;

            while (running) {
                ++rid;
                System.out.println();
                System.out.print("[JobHandlerApplication](" + rid + ")> ");
                try {
                    String s = stdin.readLine();

                    JobRequestEvent request = new JobRequestEvent();
                    request.setId(rid);
                    request.setString(s);
                    request.asyncGo(channel, Direction.DOWN);
                } catch (AppiaEventException ex) {
                    ex.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    Thread.sleep(1500);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                synchronized (this) {
                    if (!ready)
                        running = false;
                }
            }
        }
    }
}
