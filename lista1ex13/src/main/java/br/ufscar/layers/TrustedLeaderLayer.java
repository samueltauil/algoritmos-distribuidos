package br.ufscar.layers;

import br.ufscar.events.TrustEvent;
import br.ufscar.sessions.TrustedLeaderSession;
import net.sf.appia.core.Layer;
import net.sf.appia.core.Session;
import net.sf.appia.core.events.channel.ChannelInit;

/**
 * Created by samueltauil on 4/15/15.
 */
public class TrustedLeaderLayer extends Layer {

    public TrustedLeaderLayer() {
        evProvide = new Class[0];

        evRequire = new Class[0];

        evAccept = new Class[2];
        evAccept[0] = TrustEvent.class;
        evAccept[1] = ChannelInit.class;
    }

    public Session createSession() {
        return new TrustedLeaderSession(this);
    }
}