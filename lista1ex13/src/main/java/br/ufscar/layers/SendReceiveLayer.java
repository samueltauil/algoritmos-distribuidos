package br.ufscar.layers;

import br.ufscar.events.HeartBeatEvent;
import br.ufscar.events.TrustEvent;
import br.ufscar.sessions.SendReceiveSession;
import net.sf.appia.core.Layer;
import net.sf.appia.core.Session;
import net.sf.appia.core.events.channel.ChannelInit;

/**
 * Created by samueltauil on 4/15/15.
 */
public class SendReceiveLayer extends Layer {

    public SendReceiveLayer() {
        evProvide = new Class[2];
        evProvide[0] = HeartBeatEvent.class;
        evProvide[1] = TrustEvent.class;

        evRequire = new Class[0];

        evAccept = new Class[2];
        evAccept[0] = HeartBeatEvent.class;
        evAccept[1] = ChannelInit.class;
    }

    public Session createSession() {
        return new SendReceiveSession(this);
    }
}

