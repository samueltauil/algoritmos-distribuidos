package br.ufscar.sessions;

import br.ufscar.events.HeartBeatEvent;
import br.ufscar.events.TrustEvent;
import br.ufscar.model.CustomProcess;
import br.ufscar.model.ProcessList;
import net.sf.appia.core.*;
import net.sf.appia.core.events.channel.ChannelInit;
import net.sf.appia.core.message.Message;
import net.sf.appia.protocols.common.RegisterSocketEvent;

import java.io.*;
import java.net.InetSocketAddress;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by samueltauil on 4/15/15.
 */
public class SendReceiveSession extends Session {

    public SendReceiveSession(Layer layer) {
        super(layer);
    }

    public void handle(Event event) {

        if (event instanceof ChannelInit)
            handleChannelInit((ChannelInit) event);
        else if (event instanceof HeartBeatEvent)
            handleDeliver((HeartBeatEvent) event);
    }

    private Timer timer = null;
    private ProcessList processes;
    private ProcessList candidates;
    private CustomProcess leader = null;
    private Channel channel = null;

    private long delta;

    private void handleChannelInit(ChannelInit init) {
        candidates = new ProcessList();
        leader = processes.getSelf();
        channel = init.getChannel();
        delta = 5000;

        if (timer == null){
            timer = new Timer();
            timer.schedule(new Timeout(), delta);
        }

        try {
            RegisterSocketEvent rse = new RegisterSocketEvent(
                    init.getChannel(), Direction.DOWN, this);

            InetSocketAddress address = processes.getSelf()
                    .getCompleteAddress();

            rse.port = address.getPort();
            rse.localHost = address.getAddress();

            rse.go();
        } catch (AppiaEventException e1) {
            e1.printStackTrace();
        }


        TrustEvent event = new TrustEvent();
       //send event to notify others it's the leader
        event.setLeader(leader);

        try {
            event.setChannel(channel);
            event.setDir(Direction.UP);
            event.setSourceSession(this);

            event.init();
            event.go();
        } catch (AppiaEventException e) {
            e.printStackTrace();
        }

        File f = new File("epoch"+processes.getSelf().getId()+".txt");
        if( f.exists() ){
            try {

                BufferedReader input =  new BufferedReader(new FileReader(f));
                int epoch = Integer.parseInt(input.readLine());

                processes.getSelf().setEpoch(epoch + 1);
                store();


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                //in a case there is no file to read epoch
                processes.getSelf().setEpoch(0);
                store();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        HeartBeatEvent heartbeat = null;
        Message message = null;

        for( CustomProcess process : processes ){
            heartbeat = new HeartBeatEvent();
            message = new Message();

            message.pushObject(processes.getSelf());

            heartbeat.setMessage(message);
            heartbeat.setDest(process);
            heartbeat.setSendSource(processes.getSelf());

            try {
                heartbeat.setChannel(channel);
                heartbeat.setDir(Direction.DOWN);
                heartbeat.setSourceSession(this);

                heartbeat.init();
                heartbeat.go();
            } catch (AppiaEventException e) {
                e.printStackTrace();
            }
        }


        try {
            init.go();
        } catch (AppiaEventException ex) {
            ex.printStackTrace();
        }


        System.out.println("Channel is open.");
    }

    private void handleDeliver(HeartBeatEvent conf) {

        if( conf.getDir() == Direction.DOWN ){
            try {
                conf.go();
            } catch (AppiaEventException e) {
                e.printStackTrace();
            }
        } else {
            CustomProcess source = (CustomProcess) conf.getMessage().popObject();
            candidates.update(source);
        }
    }

    private void store() throws IOException{
        System.out.println("Armazenando no disco: epoch"+processes.getSelf().getId()+".txt valor:"+ processes.getSelf().getEpoch());
        FileWriter outFile = new FileWriter("epoch"+processes.getSelf().getId()+".txt", false);
        PrintWriter out = new PrintWriter(outFile);

        out.print(processes.getSelf().getEpoch());
        out.close();
    }

    public void init(ProcessList processes) {
        this.processes = processes;
    }

    private class Timeout extends TimerTask {

        @Override
        public void run() {
            if(!candidates.isEmpty()){
                CustomProcess newleader = candidates.selectLeader();
                if( newleader.getId() != leader.getId() ){
                    delta += delta;
                    leader = newleader;

                    TrustEvent event = new TrustEvent();
                    event.setLeader(leader);
                    try {
                        event.asyncGo(channel, Direction.UP);
                    } catch (AppiaEventException e) {
                        e.printStackTrace();
                    }
                }
            }
            HeartBeatEvent heartbeat = null;
            Message message = null;

            for( CustomProcess process : processes ){
                heartbeat = new HeartBeatEvent();
                message = new Message();

                message.pushObject(processes.getSelf());

                heartbeat.setMessage(message);
                heartbeat.setDest(process);
                heartbeat.setSendSource(processes.getSelf());

                try {
                    heartbeat.asyncGo(channel, Direction.DOWN);
                } catch (AppiaEventException e) {
                    e.printStackTrace();
                }

            }

            candidates.clear();
            timer.schedule(new Timeout(), delta);
        }

    }
}