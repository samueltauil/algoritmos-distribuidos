package br.ufscar.sessions;

import br.ufscar.events.TrustEvent;
import net.sf.appia.core.AppiaEventException;
import net.sf.appia.core.Event;
import net.sf.appia.core.Layer;
import net.sf.appia.core.Session;
import net.sf.appia.core.events.channel.ChannelInit;

/**
 * Created by samueltauil on 4/15/15.
 */
public class TrustedLeaderSession extends Session {

    public TrustedLeaderSession(Layer layer) {
        super(layer);
    }

    public void handle(Event event) {
        if (event instanceof ChannelInit)
            handleChannelInit((ChannelInit) event);
        else if (event instanceof TrustEvent)
            handleTrust((TrustEvent) event);
    }

    private void handleTrust(TrustEvent event) {
        System.out.println("Lider Atual: [Processo: " + event.getLeader().getId()+"]");
    }

    private void handleChannelInit(ChannelInit init) {

        try {
            init.go();
        } catch (AppiaEventException ex) {
            ex.printStackTrace();
        }
    }



}