package br.ufscar.execution;

import br.ufscar.layers.SendReceiveLayer;
import br.ufscar.layers.TrustedLeaderLayer;
import br.ufscar.model.CustomProcess;
import br.ufscar.model.ProcessList;
import br.ufscar.sessions.SendReceiveSession;
import net.sf.appia.core.*;
import net.sf.appia.protocols.udpsimple.UdpSimpleLayer;

/**
 * Created by samueltauil on 4/15/15.
 */
public class Execution {

    private static int PROC_ID_SENDER = 0;
    private static int PROC_ID_RECEIVER = 1;
    private static String ADDR_SENDER = "localhost";
    private static String ADDR_RECEIVER = "localhost";
    private static int PORT_SENDER = 8080;
    private static int PORT_RECEIVER = 9090;

    public static void main(String[] args) {

        if ( args == null || args.length == 0 || args.length > 1 ){
            System.out.println("Invalid arguments.");
            System.exit(0);
        }

	    /* Create layers and put them on a array */
        Layer[] qos = {new UdpSimpleLayer(), new SendReceiveLayer(), new TrustedLeaderLayer()};

	    /* Create a QoS */
        QoS myQoS = null;
        try {
            myQoS = new QoS("UDP Simple Example", qos);
        } catch (AppiaInvalidQoSException ex) {
            System.err.println("Invalid QoS");
            System.err.println(ex.getMessage());
            System.exit(1);
        }

        Channel channel = myQoS
                .createUnboundChannel("UDP Simple Channel");

        SendReceiveSession sas = (SendReceiveSession) qos[qos.length - 2]
                .createSession();
        sas.init(buildProcessSet(args[0]));
        ChannelCursor cc = channel.getCursor();

        try {
            cc.top();
            cc.down();
            cc.setSession(sas);
        } catch (AppiaCursorException ex) {
            System.err.println("Unexpected exception in main. Type code:" + ex.type);
            ex.printStackTrace();
            System.exit(1);
        }

        try {

            channel.start();
        } catch (AppiaDuplicatedSessionsException ex) {
            System.err.println("Error in start");
            System.exit(1);
        }

        System.out.println("Starting Appia...");
        Appia.run();
    }

    private static ProcessList buildProcessSet(String arg) {
        int current = Integer.parseInt(arg);
        ProcessList processes = new ProcessList();

        processes.add(new CustomProcess(PROC_ID_RECEIVER, PORT_RECEIVER, ADDR_RECEIVER));
        processes.add(new CustomProcess(PROC_ID_SENDER, PORT_SENDER, ADDR_SENDER));

        processes.getProcessById(current).setSelf(true);
        System.out.println("ID PROCESSO: " + current);
        return processes;
    }
}
