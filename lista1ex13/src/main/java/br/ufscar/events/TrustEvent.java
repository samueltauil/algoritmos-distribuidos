package br.ufscar.events;

import br.ufscar.model.CustomProcess;
import net.sf.appia.core.Event;

/**
 * Created by samueltauil on 4/15/15.
 */
public class TrustEvent extends Event {
    CustomProcess leader = null;

    public CustomProcess getLeader() {
        return leader;
    }

    public void setLeader(CustomProcess leader) {
        this.leader = leader;
    }
}
