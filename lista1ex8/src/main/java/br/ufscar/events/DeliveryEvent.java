package br.ufscar.events;

import br.ufscar.model.CustomProcess;
import net.sf.appia.core.events.SendableEvent;

/**
 * Created by samueltauil on 4/8/15.
 */
public class DeliveryEvent extends SendableEvent {

    private CustomProcess processDest;
    private CustomProcess processSource;

    public CustomProcess getDestProcess() {
        return processDest;
    }

    public CustomProcess getSourceProcess() {
        return processSource;
    }

    public void setDestProcess(CustomProcess process) {
        this.processDest = process;

        dest = process.getCompleteAddress();
    }

    public void setSourceProcess(CustomProcess process) {
        this.processSource = process;

        source = process.getCompleteAddress();
    }

}
