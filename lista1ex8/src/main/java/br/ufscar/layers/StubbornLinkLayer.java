package br.ufscar.layers;

import br.ufscar.events.DeliveryEvent;
import br.ufscar.events.SendEvent;
import br.ufscar.sessions.StubbornLinkSession;
import net.sf.appia.core.Layer;
import net.sf.appia.core.Session;
import net.sf.appia.core.events.channel.ChannelInit;

/**
 * Created by samueltauil on 4/8/15.
 */
public class StubbornLinkLayer extends Layer {

    public StubbornLinkLayer() {
        evProvide = new Class[0];

        evRequire = new Class[0];

        evAccept = new Class[3];
        evAccept[0] = DeliveryEvent.class;
        evAccept[1] = ChannelInit.class;
        evAccept[2] = SendEvent.class;
    }


    @Override
    public Session createSession() {
        return new StubbornLinkSession(this);
    }
}
