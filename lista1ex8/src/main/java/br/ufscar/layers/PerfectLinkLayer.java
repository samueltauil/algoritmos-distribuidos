package br.ufscar.layers;

import br.ufscar.events.DeliveryEvent;
import br.ufscar.events.SendEvent;
import br.ufscar.sessions.PerfectLinkSession;
import net.sf.appia.core.Layer;
import net.sf.appia.core.Session;
import net.sf.appia.core.events.channel.ChannelInit;

/**
 * Created by samueltauil on 4/8/15.
 */
public class PerfectLinkLayer extends Layer {

    public PerfectLinkLayer() {
        evProvide = new Class[1];
        evProvide[0] = SendEvent.class;

        evRequire = new Class[0];

        evAccept = new Class[2];
        evAccept[0] = DeliveryEvent.class;
        evAccept[1] = ChannelInit.class;
    }

    @Override
    public Session createSession() {
        return new PerfectLinkSession(this);
    }
}
