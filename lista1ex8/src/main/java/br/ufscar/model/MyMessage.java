package br.ufscar.model;

import java.io.Serializable;

/**
 * Created by samueltauil on 4/8/15.
 */
public class MyMessage implements Serializable {

    private static final long serialVersionUID = -7930361006470891430L;
    private int id;
    private String string;

    public MyMessage(int id, String string) {
        this.id = id;
        this.string = string;
    }

    public int getId(){
        return id;
    }

    public String getString(){
        return string;
    }

}
