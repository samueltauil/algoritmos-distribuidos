package br.ufscar.sessions;

import br.ufscar.events.DeliveryEvent;
import br.ufscar.events.SendEvent;
import br.ufscar.model.ProcessList;
import net.sf.appia.core.*;
import net.sf.appia.core.events.channel.ChannelInit;
import net.sf.appia.core.message.Message;
import net.sf.appia.protocols.common.RegisterSocketEvent;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by samueltauil on 4/8/15.
 */
public class StubbornLinkSession extends Session {
    public StubbornLinkSession(Layer layer) {
        super(layer);
    }

    public void handle(Event event) {
        if (event instanceof ChannelInit)
            handleChannelInit((ChannelInit) event);
        else if (event instanceof SendEvent)
            handleSendEvent((SendEvent) event);
        else if (event instanceof DeliveryEvent)
            handleDeliverEvent((DeliveryEvent) event);
    }

    private ProcessList processes;
    private ArrayList<Message> sent = null;
    private Timer timer = new Timer();
    private Channel channel = null;

    private void handleChannelInit(ChannelInit init) {
        channel = init.getChannel();
        sent = new ArrayList<Message>();
        timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                if( !sent.isEmpty() ){
                    sendMessage();
                }
            }
        }, 0, 5000);


        try {
            init.go();
        } catch (AppiaEventException ex) {
            ex.printStackTrace();
        }


        try {
            RegisterSocketEvent rse = new RegisterSocketEvent(init.getChannel(),
                    Direction.DOWN, this);

            InetSocketAddress address = processes.getSelf().getCompleteAddress();

            rse.port = address.getPort();
            rse.localHost = address.getAddress();

            rse.go();
        } catch (AppiaEventException e1) {
            e1.printStackTrace();
        }
    }

    private void handleSendEvent(SendEvent event) {
        Message message = event.getMessage();

        try {
            if( event.isOriginalMessage() ){
                sent.add((Message) message.clone());
            }

            event.go();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendMessage() {
        SendEvent event = null;

        for( Message message : sent ){
            event = new SendEvent();
            event.isOriginalMessage(false);

            event.setSourceProcess(processes.getSelf());
            event.setDestProcess(processes.getOther());

            try {
                event.setMessage((Message)message.clone());
                event.asyncGo(channel, Direction.DOWN);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void handleDeliverEvent(DeliveryEvent event) {
        try {
            event.go();
        } catch (AppiaEventException e) {
            e.printStackTrace();
        }
    }

    public void init(ProcessList processes) {
        this.processes = processes;
    }
}
