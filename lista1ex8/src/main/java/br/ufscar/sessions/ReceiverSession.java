package br.ufscar.sessions;

import br.ufscar.events.DeliveryEvent;
import br.ufscar.events.SendEvent;
import br.ufscar.model.ProcessList;
import net.sf.appia.core.*;
import net.sf.appia.core.events.channel.ChannelInit;
import net.sf.appia.core.message.Message;
import net.sf.appia.protocols.common.RegisterSocketEvent;

import java.net.InetSocketAddress;

/**
 * Created by samueltauil on 4/8/15.
 */
public class ReceiverSession extends Session {
    public ReceiverSession(Layer layer) {
        super(layer);
    }

    public void handle(Event event) {
        if (event instanceof ChannelInit)
            handleChannelInit((ChannelInit) event);
        else if (event instanceof SendEvent)
            handleSendEvent((SendEvent) event);
    }

    private ProcessList processes;

    private void handleChannelInit(ChannelInit init) {
        try {
            init.go();
        } catch (AppiaEventException ex) {
            ex.printStackTrace();
        }

        try {
            RegisterSocketEvent rse = new RegisterSocketEvent(init.getChannel(),
                    Direction.DOWN, this);

            InetSocketAddress address = processes.getSelf().getCompleteAddress();

            rse.port = address.getPort();
            rse.localHost = address.getAddress();

            rse.go();
        } catch (AppiaEventException e1) {
            e1.printStackTrace();
        }
    }

    private void handleSendEvent(SendEvent conf) {
        Message message = conf.getMessage();
        int id = message.popInt();

        System.out.println("[Message received: "
                + message.peekString() + "]");

        message.pushInt(id);

        DeliveryEvent event = new DeliveryEvent();
        event.setMessage(message);
        event.setDestProcess(processes.getOther());
        event.setSourceProcess(processes.getSelf());
        event.setChannel(conf.getChannel());
        event.setDir(Direction.DOWN);
        event.setSourceSession(this);

        try {
            event.init();
            event.go();
        } catch (AppiaEventException ex) {
            ex.printStackTrace();
        }
    }

    public void init(ProcessList processes) {
        this.processes = processes;
    }
}
