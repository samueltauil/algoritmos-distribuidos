package br.ufscar.sessions;

import br.ufscar.events.DeliveryEvent;
import br.ufscar.events.SendEvent;
import br.ufscar.model.ProcessList;
import net.sf.appia.core.*;
import net.sf.appia.core.events.channel.ChannelInit;
import net.sf.appia.core.message.Message;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by samueltauil on 4/8/15.
 */
public class PerfectLinkSession extends Session {
    public PerfectLinkSession(Layer layer) {
        super(layer);
    }

    public void handle(Event event) {
        if (event instanceof ChannelInit)
            handleChannelInit((ChannelInit) event);
        else if (event instanceof DeliveryEvent)
            handleDeliverEvent((DeliveryEvent) event);
    }

    private MessageReader reader = null;
    private ProcessList processes;
    private ArrayList<Message> delivered = null;

    private void handleChannelInit(ChannelInit init) {
        try {
            init.go();
        } catch (AppiaEventException ex) {
            ex.printStackTrace();
        }

        if (reader == null)
            reader = new MessageReader(init.getChannel());

        delivered = new ArrayList<Message>();
    }

    private void handleDeliverEvent(DeliveryEvent conf) {
        Message message = conf.getMessage();
        boolean messageDelivered = false;

        for(Message m : delivered){
            if( message.peekInt() == m.peekInt() ){
                messageDelivered = true;
                break;
            }
        }

        if(!messageDelivered){
            System.out.println("[Deliver: message delivered: "
                    + message.peekInt() + "]");

            delivered.add(message);
        }

        try {
            conf.go();
        } catch (AppiaEventException ex) {
            ex.printStackTrace();
        }
    }

    private class MessageReader extends Thread {

        public boolean ready = false;
        public Channel channel;
        private BufferedReader stdin = new BufferedReader(
                new InputStreamReader(System.in));
        private int rid = 0;

        public MessageReader(Channel channel) {
            ready = true;
            if (this.channel == null)
                this.channel = channel;
            this.start();
        }

        public void run() {
            boolean running = true;

            while (running) {
                rid++;
                System.out.println();
                System.out.print("[Sender](" + rid + ")> ");
                try {
                    String s = stdin.readLine();

                    SendEvent request = new SendEvent();
                    request.isOriginalMessage(true);

                    Message m = new Message();
                    m.pushString(s);
                    m.pushInt(rid);
                    request.setMessage(m);

                    request.setDestProcess(processes.getOther());
                    request.setSourceProcess(processes.getSelf());

                    request.asyncGo(channel, Direction.DOWN);
                } catch (AppiaEventException ex) {
                    ex.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    Thread.sleep(1500);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                synchronized (this) {
                    if (!ready)
                        running = false;
                }
            }
        }
    }

    public void init(ProcessList processes) {
        this.processes = processes;
    }
}
