package irdp.protocols.tutorialDA.eagerRB;

import irdp.protocols.tutorialDA.events.ProcessInitEvent;
import net.sf.appia.core.Layer;
import net.sf.appia.core.Session;
import net.sf.appia.core.events.SendableEvent;
import net.sf.appia.core.events.channel.ChannelClose;
import net.sf.appia.core.events.channel.ChannelInit;

/**
 * Created by samueltauil on 5/7/15.
 */
public class EagerRBLayer extends Layer {

    public EagerRBLayer(){

        evProvide = new Class[0];

        evRequire = new Class[0];

        evAccept = new Class[4];
        evAccept[0] = SendableEvent.class;
        evAccept[1] = ChannelInit.class;
        evAccept[2] = ChannelClose.class;
        evAccept[3] = ProcessInitEvent.class;
    }

    @Override
    public Session createSession() {
        return new EagerRBSession(this);
    }
}
