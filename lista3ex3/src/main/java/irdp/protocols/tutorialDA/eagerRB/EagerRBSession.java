package irdp.protocols.tutorialDA.eagerRB;

import irdp.protocols.tutorialDA.events.ProcessInitEvent;
import irdp.protocols.tutorialDA.utils.Debug;
import irdp.protocols.tutorialDA.utils.MessageID;
import irdp.protocols.tutorialDA.utils.ProcessSet;
import irdp.protocols.tutorialDA.utils.SampleProcess;
import net.sf.appia.core.*;
import net.sf.appia.core.events.SendableEvent;
import net.sf.appia.core.events.channel.ChannelInit;

import java.util.LinkedList;

/**
 * Created by samueltauil on 5/7/15.
 */
public class EagerRBSession extends Session {

    private ProcessSet processes;
    private int seqNumber;

    private LinkedList<SendableEvent>[] from;

    private LinkedList<MessageID> delivered;

    public EagerRBSession(Layer layer) {
        super(layer);
        seqNumber = 0;
    }

    public void handle(Event event) {
        // Init events. Channel Init is from Appia and ProcessInitEvent is to
        // know
        // the elements of the group
        if (event instanceof ChannelInit)
            handleChannelInit((ChannelInit) event);
        else if (event instanceof ProcessInitEvent)
            handleProcessInitEvent((ProcessInitEvent) event);
        else if (event instanceof SendableEvent) {
            if (event.getDir() == Direction.DOWN)
                // UPON event from the above protocol (or application)
                rbBroadcast((SendableEvent) event);
            else
                // UPON event from the bottom protocol (or perfect point2point
                // links)
                bebDeliver((SendableEvent) event);
        }
    }

    private void handleChannelInit(ChannelInit init) {
        try {
            init.go();
        } catch (AppiaEventException e) {
            e.printStackTrace();
        }
        delivered = new LinkedList<MessageID>();

    }

    private void handleProcessInitEvent(ProcessInitEvent event) {

        processes = event.getProcessSet();
        try {
            event.go();
        } catch (AppiaEventException e) {
            e.printStackTrace();
        }
    }


    private void rbBroadcast(SendableEvent event) {

        // first we take care of the header of the message
        SampleProcess self = processes.getSelfProcess();
        MessageID msgID = new MessageID(self.getProcessNumber(), seqNumber);
        seqNumber++;

        Debug.print("ERB: broadcasting message.");
        event.getMessage().pushObject(msgID);
        // broadcast the message
        bebBroadcast(event);

    }

    private void bebDeliver(SendableEvent event) {

        Debug.print("ERB: Received message from beb.");
        MessageID msgID = (MessageID) event.getMessage().peekObject();
        if (!delivered.contains(msgID)) {
            Debug.print("ERB: message is new.");
            delivered.add(msgID);

            SendableEvent cloned = null;
            try {
                cloned = (SendableEvent) event.cloneEvent();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
                return;
            }
            event.getMessage().popObject();
            try {
                event.go();
            } catch (AppiaEventException e) {
                e.printStackTrace();
            }

            SendableEvent retransmission = null;

            try {
                retransmission = (SendableEvent) cloned.cloneEvent();
            } catch (CloneNotSupportedException e1) {
                e1.printStackTrace();
            }
            bebBroadcast(retransmission);

        }
    }

    private void bebBroadcast(SendableEvent event) {
        Debug.print("ERB: sending message to beb.");
        try {
                event.setDir(Direction.DOWN);
                event.setSourceSession(this);
                event.init();
                event.go();
            } catch (AppiaEventException e) {
                e.printStackTrace();
            }
    }


}
